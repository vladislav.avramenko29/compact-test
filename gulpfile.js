let project_folder = "dist";
let source_foulder = "src";

let path = {
	build: {
		html: project_folder + "/",
		css: project_folder + "/css/",
		js: project_folder + "/js/",
		img: project_folder + "/img/",
		fonts: project_folder + "/fonts/",
		static: project_folder + "/static/",
	},
	src: {
		html: [source_foulder + "/*.html", "!" + source_foulder + "/_*.html"],
		css: source_foulder + "/scss/style.scss",
		js: source_foulder + "/js/*.js",
		img: source_foulder + "/img/**/*.{jpg,png,svg,gif,ico,webp}",
		fonts: source_foulder + "/fonts/*.{ttf,woff,woff2}",
		static: source_foulder + "/static/*",
	},
	watch: {
		html: source_foulder + "/**/*.html",
		css: source_foulder + "/scss/**/*.scss",
		js: source_foulder + "/js/**/*.js",
		img: source_foulder + "/img/**/*.{jpg,png,svg,gif,ico,webp}",
	},
	clean: "./" + project_folder + "/"
}

let {src, dest} = require('gulp'),
	gulp = require('gulp'),
	browsersync = require("browser-sync").create(),
	fileinclude = require("gulp-file-include"),
	del = require("del"),
	scss = require("gulp-sass"),
	autoprefixer = require("gulp-autoprefixer"),
	group_media = require("gulp-group-css-media-queries"),
	clean_css = require("gulp-clean-css"),
	rename = require("gulp-rename"),
 	uglify = require("gulp-uglify-es").default,
 	imagemin = require("gulp-imagemin");
 	//webp = require("gulp-webp"),
 	//webphtml = require("gulp-webp-html"),
 	//webpcss = require("gulp-webpcss");

function browserSync(params){
	browsersync.init({
		server:{
			baseDir: "./" + project_folder + "/"
		},
		port: 3000,
		notify:false
	})
}

function html(){
	return src(path.src.html)
		//.pipe(fileinclude())
		//.pipe(webphtml())
		.pipe(dest(path.build.html))
		.pipe(browsersync.stream())
}

function css(){
	return src(path.src.css)
		.pipe(
			scss({
				outputStyle: "expanded"
			})
		)
		.pipe(group_media())
		.pipe(
			autoprefixer({
				overrideBrowserslist: ["last 5 versions"],
				cascade: true
			})
		)
		// .pipe(
		// 	webpcss({
		// 		webpClass: '.webp',
		// 		noWebpClass: '.no-webp'
		// 	})
		// )
		.pipe(dest(path.build.css))
		.pipe(clean_css())
		.pipe(
			rename({
				extname: ".min.css"
			})
		)
		.pipe(dest(path.build.css))
		.pipe(browsersync.stream())
}

function fonts(){
	return src(path.src.fonts)
		.pipe(dest(path.build.fonts))
}

function static(){
	return src(path.src.static)
		.pipe(dest(path.build.static))
}

function js(){
	return src(path.src.js)
		//.pipe(fileinclude())
		//.pipe(dest(path.build.js))
		.pipe(uglify())
		.pipe(
			rename({
				extname: ".min.js"
			})
		)
		.pipe(dest(path.build.js))
		.pipe(browsersync.stream())
}

function images(){
	return src(path.src.img)
		// .pipe(
		// 	webp({
		// 		quality: 70
		// 	})
		// )
		//.pipe(dest(path.build.img))
		//.pipe(src(path.src.img))
		.pipe(
			imagemin({
				progressive: true,
				svgoPlugins: [{removeViewBox: false}],
				interlaced: true,
				optimizationLevel: 3
			})
		)
		.pipe(dest(path.build.img))
		.pipe(browsersync.stream())
}

function watchFiles(){
	gulp.watch([path.watch.html], html);
	gulp.watch([path.watch.css], css);
	gulp.watch([path.watch.js], js);
	gulp.watch([path.watch.img], images);
}

function clean(){
	return del(path.clean);
}

let build = gulp.series(clean, gulp.parallel(js, css, html, images, fonts, static));
let watch = gulp.parallel(build, watchFiles, browserSync);

exports.static = static;
exports.fonts = fonts;
exports.images = images;
exports.js = js;
exports.css = css;
exports.html = html;
exports.build = build;
exports.watch = watch;
exports.default = watch;
