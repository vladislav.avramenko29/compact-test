jQuery(document).ready(function ($) {
	
	$('.slider_entry').fadeIn();
	//sliders
		$('.intro_slider').slick({
			slidesToShow: 1,
			autoplay: true,
			dots: true,
		});

		$('.offer_slider').slick({
			slidesToShow: 2,
			responsive:[
				{
					breakpoint: 992,
					settings:{
						slidesToShow: 1
					}
				},
				{
					breakpoint: 576,
					settings:{
						slidesToShow: 1,
						rows: 2
					}
				}
			]
		});

		$('.product_slider').slick({
			slidesToShow: 4,
			slidesToScroll: 1,
		 	responsive: [
		    {
		      breakpoint: 1140,
		      settings: {
		        slidesToShow: 3,
		        slidesToScroll: 1,
		        infinite: true,
		      }
		    },
		    {
		      breakpoint: 880,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 1,
		        infinite: true
		      }
		    },
		    {
		      breakpoint: 576,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1,
		        infinite: true
		      }
		    }
		  ]
		})


	//submenus
		$('.equipment_submenu_link, .equipment_submenu').hover(
			function(){
				$('.equipment_submenu_link').addClass('show');
				$('.equipment_submenu').show();
			},
			function(){
				$('.equipment_submenu_link').removeClass('show');
				$('.equipment_submenu').hide();
			}
		);

		$('.parts_submenu_link, .parts_submenu').hover(
			function(){
				$('.parts_submenu_link').addClass('show');
				$('.parts_submenu').show();
			},
			function(){
				$('.parts_submenu_link').removeClass('show');
				$('.parts_submenu').hide();
			}
		);

		$('.submenu > li > a').hover(function(){
			$('.submenu_item').removeClass('show');
			$(this).siblings('.submenu_item').addClass('show');
		})

	//product count
		$('.add_count').click(function() {
	    let count = $(this).siblings('.count');
	    if(count.val() < 99){
	    	count.val(parseInt(count.val())+1);
	    }
	  });

	  $('.remove_count').click(function() {
	    let count = $(this).siblings('.count');
	    if(count.val() > 1){
	    	count.val(parseInt(count.val())-1);
	    }
	  });

	 //mobile menu
	 	$('#burger_btn').on('click', function(){
	 		$(this).toggleClass('open');
	 		$('.mobile_menu_wrapper').slideToggle();
	 		$('body').toggleClass('fixed-body');
	 	});

	 	$('.mobile_menu>li p').on('click', function(){
	 		$(this).toggleClass('show');
	 		$(this).siblings('.mobile_submenu').slideToggle();
	 	})


	 //other
	 	$('.current_lang').on('click', function(e){
	 		$('.another_lang').fadeToggle(400);
	 		return false
	 	});

	 	$('.search_btn').on('click', function(){
	 		if($(window).width() > 1139){
	 			$('.header_top_menu').toggleClass('hide');
	 			$('.search_field').fadeToggle(400);
	 		}
	 	});

});